import createPersistedState from 'vuex-persistedstate';
import _ from 'underscore';

export default ({store, isHMR}) => {
  if (isHMR) return;
  process.client && window.onNuxtReady((nuxt) => {
    createPersistedState({
      paths: [
        'index',
        'bulls-and-cows'
      ]
    })(store);
  });
}
