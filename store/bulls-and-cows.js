import _          from 'underscore';
import Step       from '~/class/bulls-and-cows/step';
import * as types from '~/mutation-types';

// state
export const state = () => ({
  hiddenNumber: '',
  settings: {
    counter: 4,
  },
  steps: [],
  inputValues: [],
  isHiddenNumberSolved: false
});

// getters
export const getters = {
  hiddenNumber: state => state.hiddenNumber,
  settings: state => state.settings,
  steps: state => state.steps,
  stepsCount: state => state.steps.length,
  inputValues: state => state.inputValues,
  isHiddenNumberSolved: state => state.isHiddenNumberSolved,
  counter: state => state.settings.counter
};

// mutations
export const mutations = {
  [types.BULLS_AND_COWS_GENERATE_HIDDEN_NUMBER](state) {
    state.hiddenNumber = _.sample(_.range(10), Number(state.settings.counter)).join('');
    console.log(`Hidden Number GENERATED ${state.hiddenNumber}`);
  },
  [types.BULLS_AND_COWS_RESET](state) {
    state.hiddenNumber         = '';
    state.steps                = [];
    state.inputValues          = [];
    state.isHiddenNumberSolved = false;
  },
  [types.BULLS_AND_COWS_PUSH_STEP](state, payload) {
    state.inputValues.push(payload.inputValue);
    state.steps.push(payload);
  },
  [types.BULLS_AND_COWS_END_GAME](state, payload) {
    state.isHiddenNumberSolved = payload;
  },
  [types.BULLS_AND_COWS_SET_SETTINGS](state, payload) {
    state.settings = payload;
  }
};

// actions
export const actions = {
  generateHiddenNumber({commit}) {
    commit(types.BULLS_AND_COWS_GENERATE_HIDDEN_NUMBER);
  },
  reset({commit}) {
    commit(types.BULLS_AND_COWS_RESET);
  },
  pushStep({state, commit, dispatch}, payload) {
    if (!state.hiddenNumber) {
      dispatch('generateHiddenNumber');
    }
    if (!payload.value) {
      dispatch('setError', 'Укажите значение для шага!', {root: true});
      return false;
    }

    if (_.find(state.inputValues, (item) => {
      return Number(item) === Number(payload.value);
    })) {
      dispatch('setNotice', 'Вы уже вводили данное значение!', {root: true});
      return false;
    }

    let step = new Step(state.hiddenNumber, state.settings, payload);

    commit(types.BULLS_AND_COWS_PUSH_STEP, step);

    if (step.isHiddenNumberSolved) {
      commit(types.BULLS_AND_COWS_END_GAME, true);
      dispatch('setSuccess', `Поздравляем, вы выйграли. Количество шагов: ${state.steps.length}!`, {root: true});
    }
  },
  setSettings({commit, dispatch}, payload) {
    commit(types.BULLS_AND_COWS_SET_SETTINGS, payload);
    dispatch('reset');
    dispatch('generateHiddenNumber');
  }
};
