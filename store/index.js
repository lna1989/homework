import * as types from '~/mutation-types';

// state
export const state = () => ({
  error: null,
  success: null,
  notice: null
});

// getters
export const getters = {
  isDark: state => state.isDark,
  error: state => state.error,
  success: state => state.success,
  notice: state => state.notice
};

// mutations
export const mutations = {
  [types.SET_SUCCESS](state, payload) {
    state.success = payload;
  },
  [types.CLEAR_SUCCESS](state) {
    state.success = null;
  },
  [types.SET_ERROR](state, payload) {
    state.error = payload;
  },
  [types.CLEAR_ERROR](state) {
    state.error = null;
  },
  [types.SET_NOTICE](state, payload) {
    state.notice = payload;
  },
  [types.CLEAR_NOTICE](state) {
    state.notice = null;
  }
};

// actions
export const actions = {
  async nuxtServerInit({dispatch}) {
  },
  setSuccess({commit}, payload) {
    commit(types.SET_SUCCESS, payload);
    setTimeout(() => commit(types.CLEAR_SUCCESS), 0);
  },
  setError({commit}, payload) {
    commit(types.SET_ERROR, payload);
    setTimeout(() => commit(types.CLEAR_ERROR), 0);
  },
  setNotice({commit}, payload) {
    commit(types.SET_NOTICE, payload);
    setTimeout(() => commit(types.CLEAR_NOTICE), 0);
  }
};
