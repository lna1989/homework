// index.js
export const SET_SUCCESS   = 'SET_SUCCESS';
export const CLEAR_SUCCESS = 'CLEAR_SUCCESS';
export const SET_ERROR     = 'SET_ERROR';
export const CLEAR_ERROR   = 'CLEAR_ERROR';
export const SET_NOTICE    = 'SET_NOTICE';
export const CLEAR_NOTICE  = 'CLEAR_NOTICE';

// bulls-and-cows.js
export const BULLS_AND_COWS_RESET                  = 'BULLS_AND_COWS_RESET';
export const BULLS_AND_COWS_PUSH_STEP              = 'BULLS_AND_COWS_PUSH_STEP';
export const BULLS_AND_COWS_GENERATE_HIDDEN_NUMBER = 'BULLS_AND_COWS_GENERATE_HIDDEN_NUMBER';
export const BULLS_AND_COWS_END_GAME               = 'BULLS_AND_COWS_END_GAME';
export const BULLS_AND_COWS_SET_SETTINGS            = 'BULLS_AND_COWS_SET_SETTINGS';
