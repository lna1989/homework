export default class Step {
  _isHiddenNumberSolved = false;
  _cowsCount             = 0;
  _bullsCount            = 0;

  // Getters
  get inputValue() {
    return this._inputValue;
  }

  get hiddenNumber() {
    return this._hiddenNumber;
  }

  get isHiddenNumberSolved() {
    return this._isHiddenNumberSolved;
  }

  get bullsCount() {
    return this._bullsCount;
  }

  get cowsCount() {
    return this._cowsCount;
  }

  get settings() {
    return this._settings;
  }

  // Setters
  set inputValue(value) {
    this._inputValue = String(value);
  }

  set hiddenNumber(value) {
    this._hiddenNumber = String(value);
  }

  set isHiddenNumberSolved(value) {
    this._isHiddenNumberSolved = Boolean(value);
  };

  set bullsCount(value) {
    this._bullsCount = Number(value);
  }

  set cowsCount(value) {
    this._cowsCount = Number(value);
  }

  set payload(value) {
    this.inputValue = value.value;

    if (this.inputValue === this.hiddenNumber) {
      this.bullsCount           = this.settings.counter;
      this.isHiddenNumberSolved = true;
      return;
    }

    this.calculateBullsAndCowsCount();
  };

  set settings(value) {
    this._settings = value;
  }

  toJSON() {
    return {
      bullsCount: this._bullsCount,
      cowsCount: this._cowsCount,
      inputValue: this._inputValue,
      isHiddenNumberSolved: this._isHiddenNumberSolved
    }
  }

  //Methods
  constructor(hiddenNumber, settings, payload) {
    this.hiddenNumber = hiddenNumber;
    this.settings     = settings;
    this.payload      = payload;
  }

  calculateBullsAndCowsCount() {
    for (let i = 0; i < this.inputValue.length; i++) {
      if (Boolean(this.inputValue[i] === this.hiddenNumber[i])) {
        this.bullsCount++;
        continue;
      }
      if (~this.hiddenNumber.indexOf(this.inputValue[i])) {
        this.cowsCount++;
      }
    }
  }
}
